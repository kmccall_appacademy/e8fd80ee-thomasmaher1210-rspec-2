def measure number_of_times = 1
  total_time = 0
  start = Time.now
  number_of_times.times { |n| yield}
  end_time = Time.now
  total_time += end_time - start
  total_time / number_of_times
end
